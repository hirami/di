import { InjectType } from './InjectType';
import { Token } from './Token';

/**
 * A token that is dependent on the current record.
 */
export interface Dependent {
	/**
	 * The token of the dependent.
	 */
	token: Token;
	/**
	 * Whether this dependent is a multi-dependency or not.
	 *
	 * @see {@link Record.multi}
	 */
	multi: boolean;
	/**
	 * How the dependency is injected inside the current record.
	 */
	injectedThrough: InjectType;
}
