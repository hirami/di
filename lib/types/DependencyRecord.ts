import { Token } from './Token';

/**
 * A data structure for dependencies of a {@link Record}.
 */
export interface DependencyRecord {
	/**
	 * The token of the dependency.
	 */
	token: Token;
	/**
	 * The property key of the injection.
	 * This is only set for property injections.
	 */
	propertyKey?: string | symbol;
	/**
	 * The parameter index of the injection.
	 * This is only set for constructor injections.
	 */
	parameterIndex?: number;
}
