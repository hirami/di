import { Type } from './Type';

/**
 * The key for all {@link Record}s.
 */
export declare type Token = string | number | symbol | bigint | Type<any>;
