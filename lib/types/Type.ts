// tslint:disable-next-line: variable-name
export const Type = Function;

/**
 * Type-Guard for {@link Type}s.
 *
 * @param v The value to check for being a type
 *
 * @returns *true* if the given value is a {@link Type}, *false* if otherwise
 */
export function isType(v: any): v is Type<any> {
	return typeof v === 'function';
}

/**
 * Represents a non-abstract class with a public constructor.
 */
// tslint:disable-next-line: callable-types
export interface Type<T> extends Function { new(...args: Array<any>): T; }
