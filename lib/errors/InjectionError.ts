import { ProcessingError } from '@ayana/errors';

/**
 * Error used when injecting a dependency failed.
 */
export class InjectionError extends ProcessingError {}
