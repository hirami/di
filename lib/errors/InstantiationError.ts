import { ProcessingError } from '@ayana/errors';

/**
 * Error used when the instantiation of a dependency fails.
 */
export class InstantiationError extends ProcessingError {}
