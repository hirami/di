import '@ayana/test';

import 'reflect-metadata';

import { Component } from './Component';
import { MetadataSymbol } from './MetadataSymbol';

autoDescribe(() => {
	it('should declare proper default component metadata on the class', function() {
		@Component() class A { }

		expect(
			Reflect.getMetadata(MetadataSymbol.COMPONENT, A).multi,
			'to be false'
		);
	});

	it('should use the given multi boolean when declaring the metadata', function() {
		@Component({ multi: true }) class A { }

		expect(
			Reflect.getMetadata(MetadataSymbol.COMPONENT, A).multi,
			'to be true'
		);
	});
});
