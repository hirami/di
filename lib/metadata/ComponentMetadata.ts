/**
 * Metadata for a component.
 */
export interface ComponentMetadata {
	/**
	 * Declares a component as a multi-dependency.
	 *
	 * @see {@link Record.multi}
	 */
	multi?: boolean;
}
