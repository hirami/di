import { ForwardRef } from '../forwardRef';
import { Token } from '../types';

/**
 * Metadata for an injection.
 */
export interface InjectMetadata {
	/**
	 * The token of the injection.
	 * If this is not set then the {@link Injector} will try to check the type metadata added by the TypeScript compiler.
	 */
	token?: Token | ForwardRef<Token>;
	/**
	 * The property key of the injection.
	 * This is only set for property injections.
	 */
	propertyKey?: string | symbol;
	/**
	 * The parameter index of the injection.
	 * This is only set for constructor injections.
	 */
	parameterIndex?: number;
}
