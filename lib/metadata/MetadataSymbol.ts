/**
 * Symbol storage for metadata keys.
 */
export class MetadataSymbol {
	/**
	 * Metadata key for storing an array of {@link InjectMetadata}
	 */
	public static INJECTIONS = Symbol('injections');
	/**
	 * Metadata key for storing an array of {@link ComponentMetadata}
	 */
	public static COMPONENT = Symbol('component');
}
