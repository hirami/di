import { ComponentMetadata } from './ComponentMetadata';
import { MetadataSymbol } from './MetadataSymbol';

/**
 * Decorator for a component.
 * This decorator is optional but can be used to attach additional metadata.
 *
 * @param meta The metadata to attach to the decorated class
 *
 * @returns *ClassDecorator*
 */
export function Component(meta?: ComponentMetadata): ClassDecorator {
	return function(target: any) {
		const metadata: ComponentMetadata = {
			multi: false,
			...meta,
		};

		metadata.multi = Boolean(metadata.multi);

		Reflect.defineMetadata(MetadataSymbol.COMPONENT, metadata, target);
	};
}
