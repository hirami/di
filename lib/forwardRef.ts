import { Token, Type } from './types';

declare type ForwardRefFn<T> = () => Type<T>;

export interface ForwardRef<T> extends ForwardRefFn<T> {
	__forward_ref__: typeof forwardRef;
}

export function forwardRef<T>(forwardRefFn: () => Type<T>): ForwardRef<T> {
	const ref = forwardRefFn as ForwardRef<T>;

	ref.__forward_ref__ = forwardRef;
	/* istanbul ignore next */
	ref.toString = function() { return 'forwardRef'; };

	return ref;
}

/**
 * @ignore
 *
 * @param v The value to be checked
 *
 * @returns *true* if the value is a {@link ForwardRef}, *false* if otherwise
 */
function isForwardRef<T>(v: Token | ForwardRef<T>): v is ForwardRef<T> {
	return typeof v === 'function' && v.hasOwnProperty('__forward_ref__') && (v as ForwardRef<T>).__forward_ref__ === forwardRef;
}

/**
 * @ignore
 *
 * @param token The token or forwardRef to resolve
 */
export function resolveForwardRef<T>(token: Type<T> | ForwardRef<T>): Type<T>;
export function resolveForwardRef<T>(token: Token | ForwardRef<T>): Token;
export function resolveForwardRef<T>(token: Token | ForwardRef<T>): Token {
	if (isForwardRef(token)) return token();

	return token;
}
