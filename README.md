@ayana/di [![NPM](https://img.shields.io/npm/v/@ayana/di.svg)](https://www.npmjs.com/package/@ayana/di) [![Discord](https://discordapp.com/api/guilds/508903834853310474/embed.png)](https://discord.gg/eaa5pYf) [![Install size](https://packagephobia.now.sh/badge?p=@ayana/di)](https://packagephobia.now.sh/result?p=@ayana/di)
===

[Documentation](https://docs.ayana.io/modules/di.html)

Dependency Injection for Bento, libraries and more

What this is
---

In a nutshell, this package provides you with a fancy way of linking objects together called "Dependency Injection" (DI). The goal of this package is to make DI portable so it can be used inside libraries too and not only applications.
This library was heavily inspired by [Angular's](https://github.com/angular/angular) way of doing DI so some terms may sound familiar.

Installation
---

With NPM

```
npm i @ayana/di
```

With Yarn

```
yarn add @ayana/di
```

Usage
---

Soon™ (Meanwhile, there are some examples inside the Injector.spec.ts file and inside the @ayana/test README)

Testing with [@ayana/test](https://www.npmjs.com/package/@ayana/test)
---

See the [README of the @ayana/test module](https://www.npmjs.com/package/@ayana/test#testing-with-ayanadi) for more information about how to test software that uses @ayana/di.

Links
---

[GitLab repository](https://gitlab.com/ayana/libs/di)

[NPM package](https://npmjs.com/package/@ayana/di)

License
---

Refer to the [LICENSE](LICENSE) file.
